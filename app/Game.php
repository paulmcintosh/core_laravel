<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PrettyBug;
use App\Game\Player;

class Game extends Model
{
    
    public $player;

    public function __construct(){

    }
    
    public function createPlayer($name){
        
        return new Player($this,$name);
    }

}
