<?php

namespace App\Http\Controllers;

use App\Flight;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Todo;

class RecordsController extends Controller {

    public function index() {

        $todo = new Todo();

        $title = $todo -> GetRecord();

        $nav = $todo -> getNav();

        $results = $todo -> getResults();

        $h1 = 'Home';

        return view('/records/index', compact('title', 'h1', 'nav', 'results'));

    }

    public function people() {

        $todo = new Todo();

        $title = $todo -> GetRecord();

        $nav = $todo -> getNav();

        $h1 = 'People';

        return view('/records/people', compact('title', 'h1', 'nav'));

    }

}
?>