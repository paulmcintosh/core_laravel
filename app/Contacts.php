<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $fillable = ['firstname','lastname','email','title'];
    public $updated_at = FALSE;
    public $created_at = FALSE;
}
