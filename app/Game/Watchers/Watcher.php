<?php

namespace App\Game\Watchers;

class Watcher implements \SplObserver{
    private $name;
   
    public function __construct($name) {
        $this->name = $name;
    }
   
    public function update(\SplSubject $subject) {
        echo $this->name.'Dominos: '.$subject->getContent().'</b><br>';
    }    
}

?>