<?php

namespace App\Game;
use App\Game\Abstracts\Character;
use App\Game\Watchers\Dominos;
use App\Game\Watched\GameStatus;
use App\PrettyBug;

class Player extends Character{
    
    public $name;    
    public $score = 0;
    public $gamestatus;
    
    
    public function __construct(\App\Game $game, $name){
        
        $this->name = $name;
        
        $this->gamestatus = new GameStatus($this->name);
        
        $current = new Dominos('Current');
        
        $previous = new Dominos('Previous');
        
        $highest = new Dominos('Highest');
        
        $lowest = new Dominos('Lowest');
        
        $this->gamestatus->attach($current);
        
        $this->gamestatus->attach($previous);
        
        $this->gamestatus->attach($highest);
        
        $this->gamestatus->attach($lowest);
        
        $this->gamestatus->broadcast('Start Game');
        
        $msg = "Player " . $this->name . " created"; 
        
        new PrettyBug($msg);
    }
    
    public function __toString(){
        return "Name: " . $this->name . 
        "| Health:" . $this->health . 
        "| Energy: " . $this->energy;
    }

}

?>