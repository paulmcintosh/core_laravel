### READ ME ###

## Technical Test Instructions ##

## Laravel Framework 7.30.4 ##



## Dominos test ##

*** http://mytest.com/game ***

* The approach I have taken for this test is to provide an overview of how to develop a game. I have focused on observerable objects which can be easily be scaled to add additional logic.
The additional logic can be player status, number of dominos, etc. By focusing on the broader strokes to scaffold out the project. This enables the finer details to be implemented
*

*** Test 1 ***
* After reviewing test the issue is base on multi-level nested views. The solution requires a change of approach. Example below
```
public function action_index()
{
    $this->layout->nest('content', View::make('home.index')->nest('submodule', 'partials.stuff'));
}
``` 
*** A psuedo template can be found in /views/contacts/nested


* Create new vhost example: http://mytest.com

* Database credentials in .env file

* Run composer to download all dependancies

* Run migration command

## Contacts Page ##

* http://mytest.com/contacts

* Navigate records with pagination menu

* Note refresh page if using search functionality

## Create Contacts Page ##

* http://mytest.com/contacts/create

* Form field validation handle by controller

