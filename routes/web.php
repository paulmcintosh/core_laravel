<?php

use Illuminate\Support\Facades\DB;

Route::get('/', function () {
  $visited = DB::select('select * from places where visited = ?', [1]);
  $togo = DB::select('select * from places where visited = ?', [0]);

  return view('travel_list', ['visited' => $visited, 'togo' => $togo ] );
});

Route::get('/records', 'RecordsController@index');

Route::get('/todo', 'TodoController@index');

Route::get('/records/people', 'RecordsController@people');

Route::get('/posts', 'PostsController@index');

Route::get('/contacts', 'ContactsController@index');

Route::get('/contacts/fetch_data', 'ContactsController@fetch_data');

Route::get('/contacts/search', 'ContactsController@search')->name('contacts.search');

Route::post('/contacts/users', 'ContactsController@create');

Route::view('/contacts/create','/contacts/create');

Route::get('/bookings/index', 'BookingsController@index');

Route::get('/game', 'GameController@index');


