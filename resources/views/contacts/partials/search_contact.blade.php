        <table class="table">
            
            <!--- thead -->
            <thead>
                
                <!-- tr -->
                <tr>
                    
                    <th>Id</th>
                    
                    <th>Firstname</th>
                    
                    <th>Lastname</th>
                    
                    <th>Email</th>
                </tr>
                <!-- tr -->
                
            </thead>
            <!-- thead -->
            
          @foreach ($contacts as $contact)
            <!-- tr -->
            <tr>
                <!-- firstname -->
                <td>{{ $contact->id }}</td>
                <!-- firstname -->
                
                <!-- firstname -->
                <td>{{ $contact->firstname }}</td>
                <!-- firstname -->
                
                <!-- lastname -->
                <td>{{ $contact->lastname }}</td>
                <!-- lastname -->
                
                <!-- email -->
                <td>{{ $contact->email }}</td>
                <!-- email -->
                
            </tr>
            <!-- tr -->
          @endforeach
        </table>
