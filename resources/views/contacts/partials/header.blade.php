<html>
    <head>
        <title>Contacts</title>
        <meta charset="utf-8">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- jquery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <!-- jquery -->
        
        <!-- script -->
        <script>
            $(document).ready(function(){
                                
                $(document).on('click','.page-link',function(event){

                   event.preventDefault();
                                       
                   var page = this.textContent;
                                       
                    fetch_data(page);
                });
                
                function fetch_data(page){
                    
                    $.ajax({
                        url:"/contacts/fetch_data?page=" + page,
                        success:function(data){
                            $('#records_data').html(data);
                        }
                    })
                }
                
                $(document).on('click','#submit',function(event){
                    
                    var query = $('#search').val();
                                        
                    fetch_customer_data(query);
                    
                });
                
                function fetch_customer_data(query = ''){
                    $.ajax({
                        url:"{{ route('contacts.search')}}",
                        method:'GET',
                        data:{query:query},
                        dataType:'json',
                        success:function(data){

                            $('#records_data').html(data.table_data);
                        }
                    });
                }
                
            });
        </script>
        <!-- script -->
    </head>
    <body>
        
        <!-- container -->
        <div class="container">