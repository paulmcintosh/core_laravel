<!-- header -->
@include('contacts.partials.create_header')
<!-- header -->

    <!-- h1 -->
    <h1>Create Contact</h1>
    <!-- h1 -->
    
    <!-- h2 -->
    <h1>Add new contact</h1>
    <!-- h2 -->
    {{$errors}}
    <form action="/contacts/users" method="POST">
        <fieldset>
            
            <div class="row">
                <label for="firstname">Firstname</label>
                <input type="text" id="firstname" name="firstname" />
            </div>
            
            <div class="row">
                <label for="lastname">lastname</label>
                <input type="text" id="lastname" name="lastname" />
            </div>

            <div class="row">
                <label for="email">Email</label>
                <input type="email" id="email" name="email" />
            </div>
            
            <div class="row">
                <label for="title">Title</label>
                <input type="text" id="title" name="title" />
            </div>
            
            {{@csrf_field()}}
            
            <div class="row">
                <button type="submit">Submit</button>
            </div>
            
        </fieldset>
    </form>    


<!-- footer -->
@include('contacts.partials.footer')
<!-- footer -->